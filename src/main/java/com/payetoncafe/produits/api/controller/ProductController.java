package com.payetoncafe.produits.api.controller;

import com.payetoncafe.produits.api.ProductsApi;
import com.payetoncafe.produits.exception.ProductException;
import com.payetoncafe.produits.model.Product;
import com.payetoncafe.produits.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

@RestController
public class ProductController implements ProductsApi {

    private static final Logger log = LoggerFactory.getLogger(ProductController.class);

    private final ProductService productService;


    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public ResponseEntity<Product> createProduct(Product product) {
        log.info("Réception de la requête de création de produit : {}", product);
        try{
            this.productService.createProduct(product);
            log.info("Fin de la requête de création de produit");
            return ResponseEntity
                    .created(URI.create("/products/" + product.getId()))
                    .body(product);
        } catch (ProductException e) {
            log.info("Fin de la requête de création de produit");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @Override
    public ResponseEntity<Void> deleteProduct(Integer productId) {
        log.info("Réception de la requête de suppresion de produit avec l'id  : {}", productId);
        try {
                this.productService.deleteProductById(productId);
                log.info("Fin de la requête de suppresion de produit");
                return ResponseEntity.ok().build();
        } catch (ProductException e) {
            log.info("Fin de la requête de suppresion de produit");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Override
    public ResponseEntity<List<Product>> getAllProducts() {
        log.info("Réception de la requête de récupération de tous les produits");
        return ResponseEntity.ok(
                this.productService.getAllProducts()
        );
    }

    @Override
    public ResponseEntity<Product> getProductById(Integer productId) {
        log.info("Réception de la requête de récupération d'un produit par ID : {}", productId);
        try {
            return ResponseEntity.ok(
                    this.productService.getProductById(productId)
            );
        } catch (ProductException e) {
            log.info("Fin de la requête de récupération d'un produit");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Override
    public ResponseEntity<Product> updateProduct(Integer productId, Product product) {
        log.info("Réception de la requête de modification d'un produit : {}", product);
        try {
            return ResponseEntity.ok(
                    this.productService.updateProduct(product)
            );
        } catch (ProductException e) {
            log.info("Fin de la requête de modification d'un produit");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
