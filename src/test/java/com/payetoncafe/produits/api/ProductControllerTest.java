package com.payetoncafe.produits.api;

import com.payetoncafe.produits.exception.ProductException;
import com.payetoncafe.produits.api.controller.ProductController;
import com.payetoncafe.produits.model.Product;
import com.payetoncafe.produits.model.ProductDetail;
import com.payetoncafe.produits.service.ProductService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProductControllerTest {

    @InjectMocks
    ProductController productController;

    @Mock
    ProductService productService;

    @Test
    void createProductTest() throws ProductException {
        Product product = initProduit(1, true);
        when(productService.createProduct(any(Product.class))).thenReturn(product);

        ResponseEntity<Product> response = productController.createProduct(product);

        verify(productService).createProduct(any(Product.class));
        assertEquals(ResponseEntity.created(URI.create("/products/" + product.getId())).body(product), response);
    }

    @Test
    void createProductKoTest() throws ProductException {
        Product product = initProduit(1, true);
        product.setName(null);
        doThrow(new ProductException("Le produit ne peut pas être créé")).when(productService).createProduct(any(Product.class));

        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () ->
            productController.createProduct(product)
        );

        assertEquals(HttpStatus.BAD_REQUEST, exception.getStatusCode());
        assertEquals("Le produit ne peut pas être créé", exception.getReason());
        verify(productService).createProduct(any(Product.class));
    }

    @Test
    void deleteProductTest() throws ProductException {
        doNothing().when(productService).deleteProductById(anyInt());

        ResponseEntity<Void> response = productController.deleteProduct(1);

        verify(productService).deleteProductById(anyInt());
        assertEquals(ResponseEntity.ok().build(), response);
    }

    @Test
    void deleteProductKoTest() throws ProductException {
        doThrow(new ProductException("Produit non trouvé")).when(productService).deleteProductById(anyInt());

        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () ->
            productController.deleteProduct(1)
        );

        assertEquals(HttpStatus.NOT_FOUND, exception.getStatusCode());
        assertEquals("Produit non trouvé", exception.getReason());
        verify(productService).deleteProductById(anyInt());
    }

    @Test
    void getAllProductsTest() {
        List<Product> productList = new ArrayList<>();
        productList.add(initProduit(1,true));
        productList.add(initProduit(2,true));

        when(productService.getAllProducts()).thenReturn(productList);

        ResponseEntity<List<Product>> response = productController.getAllProducts();

        verify(productService).getAllProducts();
        assertEquals(ResponseEntity.ok(productList), response);
    }

    @Test
    void getProductByIdTest() throws ProductException {
        Product product = initProduit(1, true);
        when(productService.getProductById(anyInt())).thenReturn(product);

        ResponseEntity<Product> response = productController.getProductById(1);

        verify(productService).getProductById(anyInt());
        assertEquals(ResponseEntity.ok(product), response);
    }

    @Test
    void getProductByIdKoTest() throws ProductException {
        doThrow(new ProductException("Produit non trouvé")).when(productService).getProductById(anyInt());

        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () ->
            productController.getProductById(1)
        );

        assertEquals(HttpStatus.NOT_FOUND, exception.getStatusCode());
        assertEquals("Produit non trouvé", exception.getReason());
        verify(productService).getProductById(anyInt());
    }

    @Test
    void updateProductTest() throws ProductException {
        Product product = initProduit(1, true);
        when(productService.updateProduct(any(Product.class))).thenReturn(product);

        ResponseEntity<Product> response = productController.updateProduct(1, product);

        verify(productService).updateProduct(any(Product.class));
        assertEquals(ResponseEntity.ok(product), response);
    }

    @Test
    void updateProductKoTest() throws ProductException {
        Product product = initProduit(1, false);
        doThrow(new ProductException("Erreur lors de la mise à jour du produit")).when(productService).updateProduct(any(Product.class));

        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () ->
            productController.updateProduct(1, product)
        );

        assertEquals(HttpStatus.BAD_REQUEST, exception.getStatusCode());
        assertEquals("Erreur lors de la mise à jour du produit", exception.getReason());
        verify(productService).updateProduct(any(Product.class));
    }

    public Product initProduit(Integer id, Boolean details) {
        ProductDetail detail = null;
        if (details) {
            detail = new ProductDetail(15.00);
        }
        Product product = new Product("Name", detail, 123);
        product.setId(id);
        return product;
    }
}
