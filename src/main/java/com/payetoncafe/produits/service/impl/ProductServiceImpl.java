package com.payetoncafe.produits.service.impl;

import com.payetoncafe.produits.exception.ProductException;
import com.payetoncafe.produits.model.Product;
import com.payetoncafe.produits.repository.ProductRepository;
import com.payetoncafe.produits.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    private static final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);


    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProducts() {
        return this.productRepository.findAll();
    }

    @Override
    public Product getProductById(Integer product_id) throws ProductException {
        return this.productRepository.findById(product_id)
                .orElseThrow(() -> new ProductException("Impossible de trouver ce produit"));
    }

    @Override
    public Product createProduct(Product product) throws ProductException {
        Product productToCreate;
        if (product.getName() != null && product.getDetails() != null
                && product.getStock() != null) {
                productToCreate = this.productRepository.save(product);
            log.info("Le produit à bien été inséré en base : {}", productToCreate);
        } else {
            log.error("Impossible de créer le produit en base avec l'id : {}", product.getId());
            throw new ProductException("Le produit ne peut pas être créer");
        }
        return productToCreate;
    }

    @Override
    public void deleteProductById(Integer product_id) throws ProductException {
        Product productToDelete = getProductById(product_id);
        this.productRepository.deleteById(productToDelete.getId());
        log.info("Le produit à bien été supprimé en base : {}", productToDelete);
    }

    @Override
    public Product updateProduct(Product product) throws ProductException {
        Product productToUpdate = this.productRepository.getReferenceById(product.getId());
        productToUpdate.setName(product.getName());
        if (product.getDetails() != null) {
            productToUpdate.setDetails(product.getDetails());
        } else {
            log.error("Impossible de modifer le produit en base avec l'id : {}", product.getId());
            throw new ProductException("Impossible de modifier le produit à cause du détail produit");
        }
        productToUpdate.setStock(product.getStock());
        productToUpdate.setOrderId(product.getOrderId());
        log.info("Le produit à bien été modifié en base : {}", productToUpdate);
        return this.productRepository.save(productToUpdate);
    }

    @Override
    public Product updateProductStock(Integer productId) throws ProductException {
        Product productToUpdate = this.getProductById(productId);
        if (productToUpdate.getStock() != 0) {
            productToUpdate.setStock(productToUpdate.getStock() - 1);
            this.productRepository.save(productToUpdate);
        } else {
            log.error("Impossible de modifier le stock du produit {} car il est deja en rupture de stock", productId);
            throw new ProductException("Impossible de modifier le stock du produit : " + productId);
        }
        return productToUpdate;
    }
}
