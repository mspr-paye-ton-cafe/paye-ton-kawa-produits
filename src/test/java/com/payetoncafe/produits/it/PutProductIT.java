package com.payetoncafe.produits.it;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.payetoncafe.produits.model.Product;
import com.payetoncafe.produits.model.ProductDetail;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class PutProductIT {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void updateProductAPI() throws Exception {

        ProductDetail detail = new ProductDetail();
        detail.setId(2);
        Product product = new Product("ProductName", detail, 123);
        product.setOrderId(1);
        product.setId(2);

        mockMvc.perform( MockMvcRequestBuilders
                        .put("/products/{id}", 2)
                        .content(asJsonString(product))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("ProductName"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.stock").value(123))
                .andExpect(MockMvcResultMatchers.jsonPath("$.orderId").value(1));
    }

    @Test
    void updateProductAPIKo() throws Exception {


        Product product = new Product("ProductName", null, 123);
        product.setOrderId(1);
        product.setId(2);

        mockMvc.perform( MockMvcRequestBuilders
                        .put("/products/{id}", 2)
                        .content(asJsonString(product))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
