package com.payetoncafe.produits;

import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;

import static org.mockito.Mockito.mockStatic;

@SpringBootTest
class ProduitsApplicationTests {

	@Test
	void contextLoads() {
	}

	@Test
	void testMainMethod() {
		try (var mockedSpringApplication = mockStatic(SpringApplication.class)) {
			ProduitsApplication.main(new String[]{});
			mockedSpringApplication.verify(() -> SpringApplication.run(ProduitsApplication.class, new String[]{}));
		}
	}

}
