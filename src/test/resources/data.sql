INSERT INTO product_detail (id, color, description, price) VALUES (2, 'red', 'redred', 13);
INSERT INTO product_detail (id, color, description, price) VALUES (3, 'blue', 'bluelbue', 35);
INSERT INTO product_detail (id, color, description, price) VALUES (4, 'green', 'green', 2);
INSERT INTO product_detail (id, color, description, price) VALUES (6, 'cyan', 'cyan', 2);
INSERT INTO product_detail (id, color, description, price) VALUES (7, 'toDelete', 'toDelete', 2);

INSERT INTO product (id, created_at, name, stock, product_detail_id, order_id) VALUES (2, '2023-08-30T03:16:28.44Z', 'ProductName', 12333, 2, 1);
INSERT INTO product (id, created_at, name, stock, product_detail_id, order_id) VALUES (3, '2023-08-30T03:16:28.44Z', 'Lapin', 1, 3, null);
INSERT INTO product (id, created_at, name, stock, product_detail_id, order_id) VALUES (8, '2023-08-30T03:16:28.44Z', 'Savon', 1, 4, 1);
INSERT INTO product (id, created_at, name, stock, product_detail_id, order_id) VALUES (13, '2023-08-30T03:16:28.44Z', 'cyan', 1, 6, 1);
INSERT INTO product (id, created_at, name, stock, product_detail_id, order_id) VALUES (14, '2023-08-30T03:16:28.44Z', 'toDelete', 1, 7, 1);