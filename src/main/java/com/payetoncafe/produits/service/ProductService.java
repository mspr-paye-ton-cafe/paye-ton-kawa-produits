package com.payetoncafe.produits.service;

import com.payetoncafe.produits.exception.ProductException;
import com.payetoncafe.produits.model.Product;

import java.util.List;

public interface ProductService {

    List<Product> getAllProducts();

    Product getProductById(Integer product_id) throws ProductException;

    Product createProduct(Product product) throws ProductException;

    void deleteProductById(Integer product_id) throws ProductException;

    Product updateProduct(Product product) throws ProductException;

    Product updateProductStock(Integer productId) throws ProductException;

}
