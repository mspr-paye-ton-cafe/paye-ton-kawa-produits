package com.payetoncafe.produits.events;

import com.payetoncafe.produits.exception.ProductException;
import com.payetoncafe.produits.service.ProductService;
import com.payetoncafe.produits.service.impl.ProductServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductListener {

    private static final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);

    private final ProductService productService;

    public ProductListener(ProductService productService) {
        this.productService = productService;
    }

    @KafkaListener(topics = "orderStock", groupId = "group_id")
    public void listenCommande(List<Integer> productIds) {
        log.info("Kafka : Réception des produits {} pour modifier leurs stocks", productIds);
        for (Integer productId : productIds) {
            try {
                log.info("Kafka : Modification du stock pour le produit {}", productId);
                this.productService.updateProductStock(productId);
            } catch (ProductException e) {
                // On ne throw pas pour continuer le traitements des autres produits en cas d'echec
                log.error("Kafka : Erreur lors de la modification du stock pour le produit {}: {}", productId, e.getMessage());
            }
        }
        log.info("Kafka : Fin de modification de stock des produits : {}", productIds);
    }
}
