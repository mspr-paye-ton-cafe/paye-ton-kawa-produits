package com.payetoncafe.produits.it;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.payetoncafe.produits.model.Product;
import com.payetoncafe.produits.model.ProductDetail;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class PostProductIT {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void createProductAPI() throws Exception {

        mockMvc.perform( MockMvcRequestBuilders
                        .post("/products")
                        .content(asJsonString(new Product("name", new ProductDetail(50.00), 123)))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists());
    }

    @Test
    void createProductAPIKo() throws Exception {

        mockMvc.perform( MockMvcRequestBuilders
                        .post("/products")
                        .content(asJsonString(new Product(null, new ProductDetail(50.00), 123)))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
