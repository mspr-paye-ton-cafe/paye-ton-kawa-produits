package com.payetoncafe.produits.exception;

public class ProductException extends Throwable {

    public ProductException(String message){
        super(message);
    }

}
