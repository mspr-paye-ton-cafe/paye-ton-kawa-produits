package com.payetoncafe.produits.it;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class DeleteProductIT {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void deleteProductAPINotFound() throws Exception {

        mockMvc.perform( MockMvcRequestBuilders.delete("/products/{id}", 50) )
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteProductAPI() throws Exception
    {
        mockMvc.perform( MockMvcRequestBuilders.delete("/products/{id}", 14) )
                .andExpect(status().isOk());
    }

}
