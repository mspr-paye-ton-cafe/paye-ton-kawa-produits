package com.payetoncafe.produits.service;

import com.payetoncafe.produits.exception.ProductException;
import com.payetoncafe.produits.model.Product;
import com.payetoncafe.produits.model.ProductDetail;
import com.payetoncafe.produits.repository.ProductRepository;
import com.payetoncafe.produits.service.impl.ProductServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class ProductServiceTest {

    @InjectMocks
    ProductServiceImpl productService;

    @Mock
    ProductRepository productRepository;

    @Test
    void getAllProductsTest() {
        //Given
        List<Product> productList = new ArrayList<>();
        Product product = initProduit(2, true);
        Product product2 = initProduit(3, true);

        productList.add(product);
        productList.add(product2);

        Mockito.when(productRepository.findAll()).thenReturn(productList);

        // When
        List<Product> ReturnedProducts = productService.getAllProducts();

        // Then
        assertEquals(ReturnedProducts.size(), productList.size());
    }

    @Test
    void getProductByIdTest() throws ProductException {
        // Given
        Product product = initProduit(2, true);
        Mockito.when(productRepository.findById(1)).thenReturn(Optional.of(product));

        // When
        Product ReturnedProduct = productService.getProductById(1);

        // Then
        assertEquals(ReturnedProduct.getName(), product.getName());
        assertEquals(ReturnedProduct.getStock(), product.getStock());
    }

    @Test
    void getProductByIdTestKO() {
        // Given
        Optional<Product> productOptional = Optional.empty();
        Mockito.when(productRepository.findById(1)).thenReturn(productOptional);

        // When
        ProductException exception = assertThrows(ProductException.class, () ->
            productService.getProductById(1)
        );

        String expectedMessage = "Impossible de trouver ce produit";
        String actualMessage = exception.getMessage();

        // Then
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void createProductTest() throws ProductException {
        // Given
        Product product = initProduit(2, true);
        Mockito.when(productRepository.save(product)).thenReturn(product);

        // When
        Product ReturnedProduct = productService.createProduct(product);

        // Then
        assertEquals(ReturnedProduct.getName(), product.getName());
        assertEquals(ReturnedProduct.getStock(), product.getStock());
    }

    @Test
    void createProductTestKO() {
        // Given
        Product product = new Product("Name", new ProductDetail(), null);

        // When
        verifyCreateException(product, "Le produit ne peut pas être créer");
    }

    @Test
    void createProductTestKO2() {
        // Given
        Product product = new Product(null, new ProductDetail(), 123);

        // When
        verifyCreateException(product, "Le produit ne peut pas être créer");
    }

    @Test
    void createProductTestKO3() {
        // Given
        Product product = initProduit(2, false);

        // When
        verifyCreateException(product, "Le produit ne peut pas être créer");
    }

    @Test
    void deleteProductByIdTest() throws ProductException {
        // Given
        Integer product_id = 1;
        Product product = initProduit(1, true);
        Mockito.when(productRepository.findById(1)).thenReturn(Optional.ofNullable(product));

        // When
        productService.deleteProductById(product_id);

        // Then
        Mockito.verify(productRepository).deleteById(product_id);
    }

    @Test
    void updateProduct() throws ProductException {
        // Given
        Product product = initProduit(2, true);

        Mockito.when(productRepository.getReferenceById(2)).thenReturn(product);
        Mockito.when(productRepository.save(product)).thenReturn(product);

        // When
        Product ReturnedProduct = productService.updateProduct(product);

        // Then
        assertEquals(ReturnedProduct.getName(), product.getName());
        assertEquals(ReturnedProduct.getStock(), product.getStock());
        assertEquals(ReturnedProduct.getDetails().getPrice(), product.getDetails().getPrice());
        assertEquals(ReturnedProduct.getOrderId(), product.getOrderId());
    }

    @Test
    void updateProductKo() {
        // Given
        Product product = initProduit(2, false);

        Mockito.when(productRepository.getReferenceById(2)).thenReturn(product);

        // When
        ProductException exception = assertThrows(ProductException.class, () ->
                productService.updateProduct(product)
        );

        String expectedMessage = "Impossible de modifier le produit à cause du détail produit";
        String actualMessage = exception.getMessage();

        // Then
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void updateProductStockTest() throws ProductException {
        // Given
        Product product = initProduit(2, true);

        Mockito.when(productRepository.findById(2)).thenReturn(Optional.of(product));
        Mockito.when(productRepository.save(product)).thenReturn(product);

        // When
        Product ReturnedProduct = productService.updateProductStock(2);

        // Then
        assertEquals(122, ReturnedProduct.getStock());
    }

    @Test
    void updateProductStockTestKo() {
        // Given
        Product product = initProduit(2, false);
        product.setStock(0);
        Mockito.when(productRepository.findById(2)).thenReturn(Optional.of(product));

        // When
        ProductException exception = assertThrows(ProductException.class, () ->
                productService.updateProductStock(2)
        );

        String expectedMessage = "Impossible de modifier le stock du produit : "+product.getId();
        String actualMessage = exception.getMessage();

        // Then
        assertTrue(actualMessage.contains(expectedMessage));
    }

    public Product initProduit(Integer id, Boolean details) {
        ProductDetail detail = null;
        if (details) {
            detail = new ProductDetail(15.00);
        }
        Product product = new Product("Name", detail, 123);
        product.setId(id);
        return product;
    }

    public void verifyCreateException(Product product, String msgErreur) {
        ProductException exception = assertThrows(ProductException.class, () ->
                productService.createProduct(product)
        );
        String actualMessage = exception.getMessage();
        // Then
        assertTrue(actualMessage.contains(msgErreur));
    }
}


